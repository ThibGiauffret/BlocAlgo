import * as Blockly from "blockly";
import { pythonGenerator } from "blockly/python";

export function setNumpyGen() {
  // Déclarations
  pythonGenerator["numpy_array"] = function (block: any) {
    var value = pythonGenerator.valueToCode(
        block,
        "array_value",
        pythonGenerator.ORDER_NONE
      );
    var code = "np.array(" + value + ")";
    return [code, pythonGenerator.ORDER_NONE];
  };

    pythonGenerator["numpy_polyfit"] = function (block: any) {
    var variable_var = pythonGenerator.variableDB_.getName(
        block.getFieldValue("x"),
        Blockly.Names.NameType.VARIABLE
        );
    var variable_var2 = pythonGenerator.variableDB_.getName(
        block.getFieldValue("y"),
        Blockly.Names.NameType.VARIABLE
        );
    var value = block.getFieldValue("deg");
    var code = "np.polyfit(" + variable_var + "," + variable_var2 + "," + value + ")";
    return [code, pythonGenerator.ORDER_NONE];
    };

}